﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace PokerGame
{
    class Hand
    {
        public List<Card> Cards = new List<Card>(5);
        public PokerHand PokerHand { get; set; }

        public Hand (Deck deck)
        {
            for (int i = 0; i < 5; i++)
            {
                Cards.Add(deck.DealCard());
            }
        }

        public void IntentifyHand(string playerName)
        {
            Console.WriteLine($"{playerName}'s cards");
            Console.WriteLine($"{Cards[0].GetCardName()} of {Cards[0].Suit}");
            Console.WriteLine($"{Cards[1].GetCardName()} of {Cards[1].Suit}");
            Console.WriteLine($"{Cards[2].GetCardName()} of {Cards[2].Suit}");
            Console.WriteLine($"{Cards[3].GetCardName()} of {Cards[3].Suit}");
            Console.WriteLine($"{Cards[4].GetCardName()} of {Cards[4].Suit}");
            IdentifyHand();
        }

        private bool IsPair()
        {
            return Cards.GroupBy(card => card.Value).Count(group => group.Count() == 2) == 1;
        }

        private bool IsTwoPair()
        {
            return Cards.GroupBy(card => card.Value).Count(value => value.Count() == 2) == 2;
        }

        private bool IsTrips()
        {
            return Cards.GroupBy(card => card.Value).Any(value => value.Count() == 3);
        }

        private bool IsFourOfAKind()
        {
            return Cards.GroupBy(card => card.Value).Any(value => value.Count() == 4);
        }

        private bool IsFullHouse()
        {
            return IsTrips() && IsTrips();
        }

        private bool IsFlush()
        {
            return Cards.GroupBy(card => card.Suit).Count() == 1;
        }

        private bool IsStraight()
        {
            return Cards.Max(card => card.Value) - Cards.Min(card => card.Value) == 4 || IsLowStraight();
        }

        private bool IsLowStraight()
        {
            return Cards.Exists(card => card.Value == 2) &&
                Cards.Exists(card => card.Value == 3) &&
                Cards.Exists(card => card.Value == 4) &&
                Cards.Exists(card => card.Value == 5) &&
                Cards.Exists(card => card.Value == 14);
        }

        private bool IsLowStraightFlush()
        {
            return IsLowStraight() && IsFlush();
        }

        private bool IsStraightFlush()
        {
            return IsStraight() && IsFlush() || IsLowStraightFlush();
        }

        private bool IsRoyalFlush()
        {
            return IsFlush() &&
                Cards.Exists(card => card.Value == 10) &&
                Cards.Exists(card => card.Value == 11) &&
                Cards.Exists(card => card.Value == 12) &&
                Cards.Exists(card => card.Value == 13) &&
                Cards.Exists(card => card.Value == 14);
        }

        public void IdentifyHand()
        {
            if (IsRoyalFlush())
            {
                PokerHand = PokerHand.ROYAL_FLUSH;
            }

            else if (IsStraightFlush())
            {
                PokerHand = PokerHand.STRAIGHT_FLUSH;
            }

            else  if (IsFourOfAKind())
            {
                PokerHand = PokerHand.FOUR_OF_A_KIND;
            }

            else if (IsFullHouse())
            {
                PokerHand = PokerHand.FULL_HOUSE;
            }

            else if (IsFlush())
            {
                PokerHand = PokerHand.FLUSH;
            }

            else if (IsStraight())
            {
                PokerHand = PokerHand.STRAIGHT;
            }

            else if (IsTrips())
            {
                PokerHand = PokerHand.TRIPS;
            }

            else if (IsTwoPair())
            {
                PokerHand = PokerHand.TWO_PAIR;
            }

            else if (IsPair())
            {
                PokerHand = PokerHand.PAIR;
            } 

            else
            {
                PokerHand = PokerHand.HIGH_CARD;
            }

        }


    }
}

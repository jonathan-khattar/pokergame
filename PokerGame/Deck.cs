﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PokerGame
{
    class Deck
    {
        public string[] Suits = new string[] { "Hearts", "Spades", "Diamonds", "Clubs" };
        public List<Card> Cards = new List<Card>();

        public Deck()
        {
            CreateDeck();
        }

        public void CreateDeck()
        {
            int index = 0;
            foreach (string suit in Suits)
            {
                for (int value = 2; value <= 14; value++)
                {
                    Card card = new Card(suit, value);
                    Cards.Add(card);
                    index++;
                }
            }
        }

        public void PrintDeck()
        {
            for (int i = 0; i < Cards.Count; i++)
            {
                Console.WriteLine($"{Cards[i].Value} {Cards[i].Suit}");
            }
        }

        public Card DealCard()
        {
            // Check if the deck has been populated
            if (Cards.Count == 0)
            {
                return null;
            }

            int index = new Random().Next(Cards.Count);
            Card card = Cards[index];
            Cards.RemoveAt(index);

            return card;
        }
    }
}

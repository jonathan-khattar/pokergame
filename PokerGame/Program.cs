﻿using System;

namespace PokerGame
{
    class Program
    {
        static void Main(string[] args)
        {
            // Generate a deck of 52 cards
            Deck deck = new Deck();

            // deal 4 hands of 5 cards
            Hand handOne = new Hand(deck);
            Hand handTwo = new Hand(deck);
            Hand handThree = new Hand(deck);
            Hand handFour = new Hand(deck);

            handOne.IntentifyHand("Player One");
            handTwo.IntentifyHand("Player Two");
            handThree.IntentifyHand("Player Three");
            handFour.IntentifyHand("Player Four");
            // Identify each hand 

            // Identify the winning hand


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PokerGame
{
    class Card
    {
        public int Value { set; get; }
        public string Suit { get; set; }

        public Card(string suit, int value)
        {
            Suit = suit;
            Value = value;
        }

        public string GetCardName ()
        {
            string result;

            switch(Value)
            {
                case 11:
                    result = "Jack";
                    break;
                case 12:
                    result = "Queen";
                    break;
                case 13:
                    result = "King";
                    break;
                case 14:
                    result = "Ace";
                    break;
                default:
                    result = Value.ToString();
                    break;
            }

            return result;
        }

    }
}
